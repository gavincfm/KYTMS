package com.kytms.shipmentTemplate.dao.impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.TemplateLedgerDetails;
import com.kytms.shipmentTemplate.dao.TemplateLedgerDTDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository(value = "TemplateLedgerDTDao")
public class TemplateLedgerDTDaoImpl extends BaseDaoImpl<TemplateLedgerDetails> implements TemplateLedgerDTDao<TemplateLedgerDetails> {
    private final Logger log = Logger.getLogger(TemplateLedgerDTDaoImpl.class);//输出Log日志
}
