package com.kytms.rule.core;

import com.kytms.rule.core.exception.RuleException;
import org.codehaus.groovy.tools.GroovyClass;

import java.util.HashMap;
import java.util.Map;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * 利用Goovy语言来执行规则
 *
 * @author 臧英明
 * @create 2018-05-07
 */
public class GroovyRule implements CacheRule<GroovyClass> {
    private static final Map<String,RuleModel> groovyClassCache = new HashMap<String, RuleModel>(); //缓存
    private GroovyRule(){}
    private static GroovyRule groovyRule = new GroovyRule();
    public static GroovyRule  getInstance(){
        return groovyRule;
    }
    public GroovyClass toCompile(String str) throws RuleException {
        return null;
    }

    public Object executeRule(RuleModel rullModel) throws RuleException {
        return null;
    }

    public Object executeRule(String id)throws RuleException {
        return null;
    }

    public boolean isExist(String id) {
        return groovyClassCache.containsKey(id);
    }

    public void saveRule(String id, RuleModel ruleModel) {
        groovyClassCache.put(id,ruleModel);
    }

    public RuleModel getRule(String id) {
        return groovyClassCache.get(id);
    }
}
